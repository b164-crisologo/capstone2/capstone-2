const express = require("express");
const router = express.Router();
const auth = require("../auth.js")

const ProductController = require("../controllers/productController.js")



//Creating a new product (Admin only)

router.post("/", auth.verify, (req,res) => {

		const data = {
			product: req.body,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		console.log(data.isAdmin)

		if(data.isAdmin){
			ProductController.addProduct(data).then(result => res.send(result));
		} else{
			res.send({auth: "You're not an admin"})
		}

	
})

//Retrieving all Product
router.get("/allActive", (req, res) =>{
	ProductController.getAllProduct().then(result => res.send(result));
});

//Retrieving a specific Product
router.get("/:productId", (req, res) => {
	ProductController.getProduct(req.params.productId).then(result => res.send(result));
})


//Updating a Product
router.put("/:productId", auth.verify, (req,res) => {
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})


//Archiving Product
router.put("/:productId/archive", auth.verify, (req,res) => {
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	ProductController.archiveProduct(req.params.productId, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})









module.exports = router;