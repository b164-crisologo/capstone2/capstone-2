const express = require("express");
const router = express.Router();
const auth = require("../auth.js")

const UserController = require("../controllers/userController")



//Registration for user
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});

//Getting all accounts Admin
router.get("/accounts", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	UserController.getAllAccount(req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})


//User Authentication(login)
router.post("/login", (req,res) => {
	UserController.loginUser(req.body).then(result => res.send(result))
});



//Making a User as admin
router.put("/:userId/makeAdmin", auth.verify, (req,res) => {
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	UserController.makeAdmin(req.params.userId).then(result => res.send(result))
	}else{
		res.send(false)
	}
})

//Making an Order
router.post("/makeOrder", auth.verify, (req,res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}
	UserController.makeOrder(data).then(result => res.send (result));
})

//Getting user's order
router.get("/:userId/getUserOrder", auth.verify, (req,res) => {
	const data = {
		userId : auth.decode(req.headers.authorization).id,
	}
	if(data.userId){
	UserController.getUserOrder(req.params.userId).then(result => res.send (result))
}else{
	res.send('Register Please :/')
}
})



//Getting All Order (ADMIN)
router.get("/getAllOrder", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	UserController.getAllOrder(req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})




module.exports = router;