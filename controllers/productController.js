const User = require("../models/User")
const Product = require("../models/Product")

const bcrypt = require("bcrypt")
const auth = require("../auth.js")


//Creating a new object (PRODUCT)
module.exports.addProduct = (reqBody) => {
	console.log(reqBody);
	
	//Create a new object
	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price,
		quantity: reqBody.product.quantity
	});

	//Saves the created object to our database 
	return newProduct.save().then((course, error)=>{
		//Course creation failed
		if(error){
			return 'error error';
		}else{
			//Course creation successful
			return 'Added new product';
		}
	})
}

//Getting all Acitve Product
module.exports.getAllProduct = () => {
	return Product.find({isAvailable: true}).then(result => {
		return result;
	})
}

//Retrieving Specific Product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result;
	})
}

//Updating Products
module.exports.updateProduct = (productId, reqBody) => {
	//specify the properties of the document to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((course,err) => { 
		//course not updated
		if(err) {
			return 'error error';
		} else {
			//product updated successfully
			return 'Product successfully updated';
		}
	})
}


//Archiving Product
module.exports.archiveProduct = (productId, reqBody) => {
	//specify the properties of the document to be updated
	let updatedStatus = {
		isAvailable : false,
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedStatus).then((product,err) => { 
		//course not updated
		if(err) {
			return false;
		} else {
			//courses updated successfully
			return 'Successfully Archived';
		}
	})
}