const User = require("../models/User")
const Product = require("../models/Product")

const bcrypt = require("bcrypt")
const auth = require("../auth.js")

//Checking Existing emails/Registration
module.exports.registerUser = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			console.log("already registered")
			return 'Email Registered Already';
		} else {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				mobileNo: reqBody.mobileNo,
				email: reqBody.email,
				//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in 
				//in order to encrypt the password
				password: bcrypt.hashSync(reqBody.password, 10)
			})

			//Saves the created object to our database
			return newUser.save().then((user, error) => {
				if(error) {
					return false;
				} else {
					return 'You are now already registered';		
							}
			})
		}
	})
}

//Getting all registered accounts
module.exports.getAllAccount = () => {
	return User.find({}).then(result => {
		return result;
	})
}





//User Authentication
module.exports.loginUser = (reqBody) => {
	//findOne it will returns the first record in the collection that matches the search criteria

	return User.findOne({ email: reqBody.email }).then(result => {
		if(result == null){
			return false;
		} else {
			//User exists    
			//the "compareSync" method is used to compare a non encrypted password from the login form to the 
			//encrypted password retrieved from the database and returns "true" or "false"
			                                             // from Login      from database
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			//If the password match
			if(isPasswordCorrect){
				//Generate an access token if true
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				//Password does not match
				return false;
			}


		}
	})
}

//Update Admin Status

module.exports.makeAdmin = (userId) => {
	let makeAdmin = {
		isAdmin : true,
	};

	return User.findByIdAndUpdate(userId, makeAdmin).then((user, error) => {
		if(error){
			return 'error';
		} else {
			return 'Congratulations! You are now an admin!';
		}
	})
}


//make an Order

module.exports.makeOrder = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.order.push({productId: data.productId})
  
		return user.save().then((user, error) =>{
			if(error){
				return false;
			} else {
				return true;
			}
		})
	});
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orderFromUser.push({userId: data.userId});

		return product.save().then((product,error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	});

	if(isUserUpdated && isProductUpdated){
		return 'Got your Order, Please wait for confirmation to pay'
	} else {
		return 'Please fill the information required'
	}
}


//Getting user's orders
module.exports.getUserOrder = (reqParams) => {
	return User.findById(reqParams).then(result => {
		return result.order
	})
}

//getting all the orders ADMIN
module.exports.getAllOrder = (reqBody) => {
	return User.find({isAdmin : false}).select("order").then(result => {
		return result;
	})
}