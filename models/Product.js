const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Please give some details"]
	},
	price: {
		type: String,
		required: [true, "Please give me a value "]
	},
	quantity: {
		type: String,
		required: [true, "How many i am?"]
	},
	
	isAvailable: {
		type: Boolean,
		default: true
	},

	orderFromUser : [{
		userId : {
			type: String,
			required: [true, "User id is required"]
		},
		orderedOn : {
			type: Date,
			default :  new Date()
		},
		status: {
			type: String,
			default: "For payment"
		}

	}]

})

module.exports = mongoose.model("Product", productSchema)